class ItemsController < ApplicationController
  before_action :set_item, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!, only:[:new]
  rescue_from ActiveRecord::RecordNotFound, with: :invalid_item


  # GET /items
  # GET /items.json
  def index
    if(params[:category_id])
      @items = Item.where(category_id: params[:category_id])
    else
      @items = Item.all
    end
    respond_to do |format|
      format.json { render json: @items }
      format.html
      format.rss
    end
  end

  # GET /items/1
  # GET /items/1.json
  def show
    respond_to do |format|
      format.html
      format.json { render json: @item }
    end
  end

  # GET /items/new
  def new
    @item = Item.new
    @categories = Category.all.by_name
  end

  # GET /items/1/edit
  def edit
    unless @item.owner?(current_user)#current_user!= @item.user
      redirect_to store_url, notice: 'To nie jest Twój przedmiot.'
    else
      @categories = Category.all.by_name
    end
  end

  # POST /items
  # POST /items.json
  def create
    @item = Item.new(item_params)
    @categories = Category.all.by_name
    @item.user = current_user

    respond_to do |format|
      if @item.save
        format.html { redirect_to @item, notice: 'Przedmiot został pomyślnie dodany.' }
        format.json { render action: 'show', status: :created, location: @item }
      else
        format.html { render action: 'new' }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /items/1
  # PATCH/PUT /items/1.json
  def update
    respond_to do |format|
      if @item.update(item_params)
        format.html { redirect_to @item, notice: 'Item was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end
  # DELETE /items/1
  # DELETE /items/1.json
  def destroy
    @item.destroy
    respond_to do |format|
      format.html { redirect_to items_url }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_item
    @item = Item.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def item_params
    params.require(:item).permit(:title, :detailed_description, :general_description, :price, :image_url, :category_id, :count, :todo)
  end

  def invalid_item
    logger.error "Attempt to access invalid_item #{params[:id]}"
    redirect_to store_url, notice: 'Przedmiot nie istnieje, przepraszamy.'
  end
end
