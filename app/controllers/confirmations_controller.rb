class ConfirmationsController <Devise::ConfirmationsController

	def new
		if user_signed_in?
			redirect_to store_url, notice: 'Jesteś zalogowany.'
			return false
		end
		super
	end

	def show
		if user_signed_in?
			redirect_to store_url, notice: 'Jesteś zalogowany.'
			return false
		end
		super
	end

end
