class StoreController < ApplicationController
	include CurrentCart
  before_action :set_cart
	def index
		@items = Item.all
		respond_to do |format|
			format.html
			format.json { render json: @items }
		end
	end
end
