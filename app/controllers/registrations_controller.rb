class RegistrationsController <Devise::RegistrationsController
	def create
		super
		if resource.save
			@cart = Cart.new
			@cart.user = resource
			@cart.save
		end
	end
end
