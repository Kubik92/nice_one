class CategoriesController < ApplicationController
  # before_action :set_item, only: [:show, :edit, :update, :destroy]

  def index
    @categories = Category.all
    render json: @categories
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  # def set_item
  #   @item = Item.find(params[:id])
  # end

  # Never trust parameters from the scary internet, only allow the white list through.
  # def item_params
  #   params.require(:item).permit(:title, :detailed_description, :general_description, :price, :image_url, :category_id)
  # end
end
