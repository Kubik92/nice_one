module CurrentCart
	extend ActiveSupport::Concern

	private

	def set_cart
		if current_user
			@cart = current_user.cart
		else @cart = nil
		end
	rescue ActiveRecord::RecordNotFound
		@cart = nil
	end

end
