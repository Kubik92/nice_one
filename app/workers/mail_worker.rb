class MailWorker
	include Sidekiq::Worker
	sidekiq_options queue: :mail
	def perform(line_item_id, current_user_id)
		@line_item = LineItem.find(line_item_id)
		@user = User.find(current_user_id)
		logger.info "sending email to #{@user.email}           - begin time  #{Time.now.strftime('%H:%M:%S.%6N')}"
		OrderNotifier.buyer(@line_item, @user).deliver
		logger.info "sending email to #{@line_item.item.user.email}    - begin time  #{Time.now.strftime('%H:%M:%S.%6N')}"
		OrderNotifier.seller(@line_item, @user).deliver
	end
end
