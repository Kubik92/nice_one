class Category < ActiveRecord::Base
	has_many :items
	scope :by_name, ->{order(:title)}
end
