class Item < ActiveRecord::Base
	has_many :line_items
	belongs_to :category
	belongs_to :user

	validates :category_id, presence: true
	validates :title, presence: true
	validates :price, presence: true, :numericality => { :greater_than => 0}
	validates :count, presence: true, :numericality => { :greater_than => 0, :less_than_or_equal_to => 99 }
	validates :general_description, presence: true
	validates :user_id, presence: true

	before_destroy :ensure_not_referenced_by_any_line_item


	def owner?(current_user)
	  current_user == self.user
	end

	private

	def ensure_not_referenced_by_any_line_item
		if line_items.empty?
			return true
		else
			errors.add(:base, 'Line Items present')
			return false
		end
	end
end
