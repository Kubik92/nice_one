class LineItemSerializer < ActiveModel::Serializer
  attributes :id
  has_one :item
  has_one :cart
end
