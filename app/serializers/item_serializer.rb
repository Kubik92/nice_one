class ItemSerializer < ActiveModel::Serializer
	attributes :id, :title, :general_description, :detailed_description, :price, :category_id, :user_email, :offers, :count, :category_title, :todo

	def user_email
		object.user.email
	end

	def category_title
		object.category.title
	end

	def todo
		if object.todo
			return "tak"
		else
			return "nie"
		end
	end
end
