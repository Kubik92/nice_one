app.factory('Category', ['railsResourceFactory', function(railsResourceFactory) {

  return railsResourceFactory( {
  	url: '/categories',
  	title: 'category'
	  });
}]);