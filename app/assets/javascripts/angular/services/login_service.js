app.factory('LoginUser', ['railsResourceFactory', function(railsResourceFactory) {

  return railsResourceFactory( {
  	url: '/users/sign_in'
  });
}]);