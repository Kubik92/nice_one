app.factory('LineItem', ['railsResourceFactory', function(railsResourceFactory) {

  return railsResourceFactory( {
  	url: '/items/{{itemId}}/line_items',
  	title: 'line_item'
	  });
}]);