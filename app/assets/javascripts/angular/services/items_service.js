app.factory('Item', ['railsResourceFactory', function(railsResourceFactory) {

  return railsResourceFactory( {
    url: '/items',
    title: 'item'
    });
}]);