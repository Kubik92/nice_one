app.controller('CategoriesController', ['$scope', 'Category', function($scope, Category) {
  
 $scope.searching = true;
    $scope.categories = [];
    $scope.fetchCategories = function(){

    Category.query().then(function (results) {
        $scope.categories = results;
        $scope.searching = false;
    }, function (error) {
        $scope.searching = false;
    });
};
}]);