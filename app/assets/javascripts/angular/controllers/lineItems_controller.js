app.controller('LineItemsController', ['$scope', '$http', '$controller', 'LineItem', function($scope, $http, $controller, LineItem) {
       $controller('ItemController', {$scope: $scope});

    $scope.addItem = function(){
    	new LineItem({itemId: window.Id}).create().then(
            function(results){
                $scope.linkUpdated = true;
             },
            function (error){
                $scope.errors = true;
                $scope.linkUpdated = false;
            }
        )
    };
}]);