app.controller('ItemsController', ['$scope', '$controller', 'Item' , function($scope, $controller, Item) {
  $controller('CategoriesController', {$scope: $scope});

    $scope.tab = null;
    $scope.color = "black";

    $scope.fetchItems = function(){
      Item.query().then(function (results) {
        $scope.items = results;
        $scope.tab = null;
    }, function (error) {
    });};

    $scope.fetchItemsForCategory = function(category){
      Item.query({category_id: category.id}).then(function (results) {
        $scope.items = results;
        $scope.tab = category.id;
    }, function (error) {
    });};
    

    $scope.fetchItems();

    $scope.categoryIsSelected = function(categoryselect){
      return $scope.tab === categoryselect
    };

    $scope.setColor = function(count, offers){
      if (count > offers){
        return "green";
      }
        else if (count < offers/1.2){
        return "red";
      }
      else{
          return "orange";
        }
    };

    $scope.setDecoration = function(count, offers, todo){
      if ( (count < offers/1.2) && todo == "nie"){
        return "line-through";
      }
    };

    // $scope.setColor = function(){
    //   return "red";
    // };

}]);
