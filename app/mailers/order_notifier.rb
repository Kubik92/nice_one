class OrderNotifier < ActionMailer::Base
  default from: "mylittlehomemadeshop@gmail.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.order_notifier.buyer.subject
  #
  def buyer(line_item, current_user)
    @line_item = line_item
    @user = current_user
    mail to: @user.email, subject: "Kupiłeś przedmiot #{@line_item.item.title}!"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.order_notifier.seller.subject
  #
  def seller(line_item, current_user)
    @line_item = line_item
    @user = current_user

    mail to: @line_item.item.user.email,subject: "Użytkownik #{@user.email} kupił przedmiot #{@line_item.item.title}!"
  end
end
