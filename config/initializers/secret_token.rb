# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
HomemadeShop::Application.config.secret_key_base = '8ac9e36dd5b024b66f6e389a6804d7bb5d2994f32876108b1b45a04ee5688d20fe35229368652f59bf17105eceb173d80c8e05215079ad2a456d6ceacc0cb6fd'
