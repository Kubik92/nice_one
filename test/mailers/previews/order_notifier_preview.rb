# Preview all emails at http://localhost:3000/rails/mailers/order_notifier
class OrderNotifierPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/order_notifier/buyer
  def buyer
    OrderNotifier.buyer
  end

  # Preview this email at http://localhost:3000/rails/mailers/order_notifier/seller
  def seller
    OrderNotifier.seller
  end

end
