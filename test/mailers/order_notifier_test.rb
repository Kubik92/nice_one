require 'test_helper'

class OrderNotifierTest < ActionMailer::TestCase
  test "buyer" do
    mail = OrderNotifier.buyer
    assert_equal "Buyer", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

  test "seller" do
    mail = OrderNotifier.seller
    assert_equal "Seller", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
