class AddCountToItems < ActiveRecord::Migration
  def change
    add_column :items, :offers, :integer, default: 0
    add_column :items, :count, :integer, default: 1
  end
end
