class AddTodoToItems < ActiveRecord::Migration
  def change
  	add_column :items, :todo, :boolean, default: false
  end
end
