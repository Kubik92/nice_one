class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :title
      t.text :detailed_description
      t.string :general_description
      t.decimal :price, precision: 8, scale: 2
      t.string :image_url
      t.integer :category_id

      t.timestamps
    end
    add_index(:items, :category_id)
  end
end
